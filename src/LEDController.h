/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, December 2017
 **************************************************************************/
#ifndef _WIFIRGB__LEDCONTROLLER_H
#define _WIFIRGB__LEDCONTROLLER_H

#include "LEDAnimation.h"
#include "LEDMode.h"
#include "RGB.h"
#include "BreatheAnimationStage.h"
#include <Arduino.h>

class LEDController {
  public:
    LEDController(HardwareSerial* USE_SERIAL, int redPin, int greenPin, int bluePin);
    ~LEDController();
    LEDMode getMode();
    LEDAnimation getCurrentAnimation();
    void setFixedColor(RGB rgb);
    void setFixedColor(float r, float g, float b);
    void breathe(RGB colorOne, RGB colorTwo, unsigned int speed);
    void blink(RGB colorOne, RGB colorTwo, unsigned int speed);
    void transition(RGB color, unsigned int speed);
    void hueCascade(RGB colors[], unsigned int speed);
    RGB getCurrentRGB();

    void run();

  private:
    RGB curRGB;
    int ledR, ledG, ledB;
    LEDMode curMode;
    LEDAnimation curAnimation;
    BreatheAnimationStage breatheAnimationStage;
    HardwareSerial* serialInterface;
    unsigned int lastRunMs;
    unsigned short operatingFreq;

    void setLED(float r, float g, float b);
    void setLED(RGB rgb);
};

#endif
