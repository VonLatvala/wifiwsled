/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, December 2017
 **************************************************************************/
#ifndef _WIFIRGB__LEDCONTROLLER_CPP
#define _WIFIRGB__LEDCONTROLLER_CPP

#include <Arduino.h>
#include "LEDController.h"

LEDController::LEDController(HardwareSerial* serialInterface, int redPin, int greenPin, int bluePin) {
  this->ledR = redPin;
  this->ledG = greenPin;
  this->ledB = bluePin;
  this->serialInterface = serialInterface;
  this->lastRunMs = 0;
  this->operatingFreq = 100;

  serialInterface->print("Initializing LEDs...");

  pinMode(ledR, OUTPUT);
  serialInterface->print(".");
  pinMode(ledG, OUTPUT);
  serialInterface->print(".");
  pinMode(ledB, OUTPUT);
  serialInterface->print(".");
  serialInterface->println("OK!");
  this->setLED(1.0f, 1.0f, 1.0f);
}

LEDController::~LEDController() {

}

void LEDController::setLED(float red, float green, float blue) {
  analogWrite(this->ledR, red*PWMRANGE);
  analogWrite(this->ledG, green*PWMRANGE);
  analogWrite(this->ledB, blue*PWMRANGE);

  this->curRGB.red = red;
  this->curRGB.green = green;
  this->curRGB.blue = blue;
}

void LEDController::setLED(RGB rgb) {
  this->setLED(rgb.red, rgb.green, rgb.blue);
}


LEDMode LEDController::getMode() {
  return this->curMode;
}

LEDAnimation LEDController::getCurrentAnimation() {
  return this->curAnimation;
}

void LEDController::setFixedColor(RGB rgb) {
  this->curMode = LEDMode::fixed;
  this->setLED(rgb);
}

void LEDController::setFixedColor(float r, float g, float b) {
  this->setFixedColor(RGB{r, g, b});
}

void LEDController::breathe(RGB colorOne, RGB colorTwo, unsigned int speed) {
  this->serialInterface->println(String("Starting breathe animation with speed ")+String(speed)+String(" between colors (")+String(colorOne.red)+String(",")+String(colorOne.green)+String(",")+String(colorOne.blue)+String("), (")+String(colorTwo.red)+String(",")+String(colorTwo.green)+String(",")+String(colorTwo.blue)+String(")"));
  this->breatheAnimationStage = BreatheAnimationStage{colorOne, colorTwo, 0, speed};
  this->curMode = LEDMode::animated;
  this->curAnimation = LEDAnimation::breathe;
}

void LEDController::blink(RGB colorOne, RGB colorTwo, unsigned int speed) {

}

void LEDController::transition(RGB color, unsigned int speed) {

}

void LEDController::hueCascade(RGB colors[], unsigned int speed) {

}

RGB LEDController::getCurrentRGB() {
  return this->curRGB;
}

void LEDController::run() {
  if((millis() - this->lastRunMs)/1000 < (1/this->operatingFreq)) {
    return;
  }
  if(this->curMode == fixed) {
    // Do nothing
  } else if(this->curMode == animated) {
    LEDAnimation curAnimation = this->curAnimation;
    if(curAnimation == LEDAnimation::breathe) {
      // stage = int [0,1000[; @ 0 = color1, @ 500 = color2, @ 1000 => 0 => color1

      float x = (float)breatheAnimationStage.speed;

      float dr = breatheAnimationStage.color2.red - breatheAnimationStage.color1.red;
      float dg = breatheAnimationStage.color2.green - breatheAnimationStage.color1.green;
      float db = breatheAnimationStage.color2.blue - breatheAnimationStage.color1.blue;

      float ar = dr / x;
      float ag = dg / x;
      float ab = db / x;
      //this->serialInterface->print(String(breatheAnimationStage.currentStage)+String(";"));
      if(breatheAnimationStage.currentStage <= x) {
        // From color1 -> color2
        this->setLED(breatheAnimationStage.color1.red + breatheAnimationStage.currentStage * ar,
                     breatheAnimationStage.color1.green + breatheAnimationStage.currentStage * ag,
                     breatheAnimationStage.color1.blue + breatheAnimationStage.currentStage * ab);
        breatheAnimationStage.currentStage++;
      } else { // if(currentStage >= x)
        // From color2 -> color1
        this->setLED(breatheAnimationStage.color2.red - (breatheAnimationStage.currentStage - x) * ar,
                     breatheAnimationStage.color2.green - (breatheAnimationStage.currentStage - x) * ag,
                     breatheAnimationStage.color2.blue - (breatheAnimationStage.currentStage - x) * ab);
        breatheAnimationStage.currentStage++;
      }
      if(breatheAnimationStage.currentStage == x*2+1) {
        breatheAnimationStage.currentStage = 0;
      }
    } else if(curAnimation == LEDAnimation::blink) {

    } else if(curAnimation == LEDAnimation::transition) {

    } else if(curAnimation == LEDAnimation::multiColor) {

    } else {
      // ???
    }
  } else {
    // ???
  }
  this->lastRunMs = millis();
}

#endif
