/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, December 2017
 **************************************************************************/
#ifndef _WIFIRGB__BREATHEANIMATIONSTAGE_H
#define _WIFIRGB__BREATHEANIMATIONSTAGE_H

#include "RGB.h"

struct BreatheAnimationStage {
  RGB color1, color2;
  unsigned int currentStage, speed;
};

#endif
