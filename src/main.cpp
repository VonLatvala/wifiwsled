/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, December 2017
 **************************************************************************/
#include "main.h"
// Service config

ESP8266WebServer server(80);
WebSocketsServer webSocket(WEBSOCKETPORT);
WiFiUDP Udp;
char udpPacketBuf[UDP_PACKET_BUFFER_SIZE];

// System config

String wifiSsid;
String wifiPassword;
String hostName;

// LED Config

LEDController ledController = LEDController(&USE_SERIAL, D7, D6, D5);

RGB rgbColorWhite = {1.0, 1.0, 1.0};
RGB rgbColorRed = {1.0, 0.0, 0.0};
RGB rgbColorGreen = {0.0, 1.0, 0.0};
RGB rgbColorBlue = {0.0, 0.0, 1.0};

RGB rgbColorYellow = {1.0, 0.1, 0.0};
RGB rgbColorLightYellow = {1.0, 0.8, 0.0};
RGB rgbColorDarkYellow = {1.0, 0.6, 0.0};
RGB rgbColorLightTeal = {0.0, 1.0, 0.8};
RGB rgbColorDarkTeal = {0.0, 1.0, 0.6};

RGB otaFailColor = {1.0, 0.4, 0.0};

RGB loadingColor1 = rgbColorLightYellow;
RGB loadingColor2 = rgbColorDarkYellow;
RGB successColor = rgbColorGreen;
RGB failureColor = rgbColorRed;

void setup() {
  USE_SERIAL.begin(1843200);
  analogWriteFreq(125);

  USE_SERIAL.println(String("\n") + String(SYSNAME) + String(" booting, please stand by..."));

  ledController.setFixedColor(loadingColor1);
  #if !SKIP_SPIFFS
  USE_SERIAL.print(String("Mounting FS..."));
  if(SPIFFS.begin()) {
    USE_SERIAL.println("OK!");
    ledController.setFixedColor(successColor);
    delay(100);
  } else {
    USE_SERIAL.println("FAILED.");
    ledController.setFixedColor(failureColor);
    return;
  }
  #endif

  hostName = String(ESP.getChipId());
  wifiSsid = String("NO_CONFIG");
  wifiPassword = String("NOCONFIG");

  #if !SKIP_SPIFFS
  ledController.setFixedColor(loadingColor1);
  USE_SERIAL.println("Enumerating directory /");
  Dir dir = SPIFFS.openDir("/");
  while(dir.next()) {
      USE_SERIAL.print(dir.fileName());
      File f = dir.openFile("r");
      USE_SERIAL.println(f.size());
      f.close();
  }
  USE_SERIAL.print("Checking for existing configuration on FS...");
  if(SPIFFS.exists("/config.json")) {
    ledController.setFixedColor(successColor);
    delay(100);
    ledController.setFixedColor(loadingColor1);
    USE_SERIAL.println("exists.");
    USE_SERIAL.print("Loading configuration from FS...");
    File configFh = SPIFFS.open("/config.json", "r");
    if(!configFh) {
      ledController.setFixedColor(failureColor);
      return;
      USE_SERIAL.println("FAILED.");
    } else {
      ledController.setFixedColor(successColor);
      delay(100);
      ledController.setFixedColor(loadingColor1);
      USE_SERIAL.println("OK!");
      USE_SERIAL.print("Filesize of our config: ");
      size_t configSize = configFh.size();
      USE_SERIAL.print(configSize);
      if(configSize <= MAX_CONFIG_FILESIZE) {
        USE_SERIAL.println(String(" which IS within limits (") + String(MAX_CONFIG_FILESIZE) + String("), proceeding."));
        ledController.setFixedColor(successColor);
        delay(100);
        ledController.setFixedColor(loadingColor1);
      } else {
        ledController.setFixedColor(failureColor);
        return;
        USE_SERIAL.println(String(" which IS NOT within limits(") + String(MAX_CONFIG_FILESIZE) + String("), ABORTING."));
      }
      USE_SERIAL.print("Parsing configuration...");
      std::unique_ptr<char[]> buf(new char[configSize]);
      configFh.readBytes(buf.get(), configSize);

      StaticJsonBuffer<MAX_CONFIG_FILESIZE> jsonBuffer;
      JsonObject& configJson = jsonBuffer.parseObject(buf.get());

      if(configJson.success()) {
        ledController.setFixedColor(successColor);
        delay(100);
        ledController.setFixedColor(loadingColor1);
        USE_SERIAL.println("OK!");
        wifiSsid = String((const char*)configJson["wifi"]["ssid"]);
        wifiPassword = String((const char*)configJson["wifi"]["password"]);
        hostName = String((const char*)configJson["hostname"]);
      } else {
        USE_SERIAL.println("FAILED.");
        ledController.setFixedColor(failureColor);
        return;
      }
    }
  } else {
    USE_SERIAL.println("not found :(");
    return;
  }
  #else
  USE_SERIAL.println("Skipped SPIFFS, using hardcoded defaults...");
  wifiSsid = String(DEFAULT_WIFISSID);
  wifiPassword = String(DEFAULT_WIFIPWD);
  hostName = String(DEFAULT_HOSTNAME);
  #endif

  WiFi.mode(WIFI_STA);
  WiFi.hostname(hostName);
  WiFi.begin(wifiSsid.c_str(), wifiPassword.c_str());

  USE_SERIAL.print(String("Connecting to WiFi (") + String(wifiSsid) + String(")"));

  const int connectTimeoutSec = 20;
  int elapsedConnectDurationMs = 0;
  int nthLoop = 0;
  while (WiFi.status() != WL_CONNECTED) {
    elapsedConnectDurationMs += 250;
    RGB curColor;
    if(nthLoop % 2 == 0) {
      curColor = loadingColor1;
    } else {
      curColor = loadingColor2;
    }
    ledController.setFixedColor(curColor);
    USE_SERIAL.print(".");
    if(elapsedConnectDurationMs / 1000 >= connectTimeoutSec) {
      ledController.setFixedColor(failureColor);
      delay(500);
      USE_SERIAL.println("TIMEOUT");
      USE_SERIAL.println("Rebooting...");
      ESP.restart();
    }
    nthLoop++;
    delay(250);
  }

  ledController.setFixedColor(successColor);
  delay(100);
  ledController.setFixedColor(loadingColor1);
  USE_SERIAL.println("OK!");
  USE_SERIAL.print("IP address: ");
  USE_SERIAL.println(WiFi.localIP());


  #ifdef USEOTA
    // OTA update
    USE_SERIAL.print("Starting OTA&MDNS service with hostname '" + String(hostName) + String(".local'..."));
    ArduinoOTA.onStart([]() {
      String type;

      if (ArduinoOTA.getCommand() == U_FLASH) {
        type = "sketch";
      } else { // U_SPIFFS
        type = "filesystem";
        SPIFFS.end();
      }
      Serial.println("Start updating " + type);
    });

    ArduinoOTA.onEnd([]() {
      Serial.println("\nEnd");
    });

    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });

    ArduinoOTA.onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);

      if (error == OTA_AUTH_ERROR)
        Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR)
        Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR)
        Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR)
        Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR)
        Serial.println("End Failed");
    });

    ArduinoOTA.setHostname(hostName.c_str());
    ArduinoOTA.begin();
    yield();
    ledController.setFixedColor(successColor);
    delay(100);
    ledController.setFixedColor(loadingColor1);
    USE_SERIAL.println("OK!");
  #endif

  #ifndef USEOTA
    // ArduinoOTA insists on starting the MDNS service itself, with no exceptions, so let's bend over
  USE_SERIAL.print(String("Starting MDNS responder for '") + String(hostName) + String(".local'..."));
  if(MDNS.begin(hostName.c_str())) {
  #else
  USE_SERIAL.print(String("Configuring MDNS services."));
  #endif

    USE_SERIAL.print(".");
    MDNS.addService("http", "tcp", 80);
    USE_SERIAL.print(".");
    MDNS.addService("ws", "tcp", WEBSOCKETPORT);
    USE_SERIAL.println("OK!");
    ledController.setFixedColor(successColor);
    delay(100);
    ledController.setFixedColor(loadingColor1);
  #ifndef USEOTA
  } else {
    ledController.setFixedColor(otaFailColor);
    delay(500);
    ledController.setFixedColor(loadingColor1);
    USE_SERIAL.println("FAIL");
  }
  #endif

  USE_SERIAL.print(String("Initializing HTTP Service on port ") + String(HTTPPORT) + String("..."));
  server.on("/", handleRoot);
  USE_SERIAL.print(".");
  server.onNotFound(handleNotFound);
  USE_SERIAL.print(".");
  server.on("/api/state", handleGetState);
  USE_SERIAL.print(".");
  server.on("/api/setColor", handleApiSetColor);
  USE_SERIAL.print(".");
  server.on("/api/rssi", handleRSSI);
  USE_SERIAL.print(".");
  server.on("/api/animation/breathe", handleApiAnimationBreathe);
  USE_SERIAL.print(".");
  server.on("/api/system/localIp", handleApiSysGetLocalIp);
  USE_SERIAL.print(".");
  server.begin();
  USE_SERIAL.print(".");
  ledController.setFixedColor(successColor);
  delay(100);
  ledController.setFixedColor(loadingColor1);
  USE_SERIAL.println("OK!");

  USE_SERIAL.print(String("Initializing WebSocketServer on port ") + String(WEBSOCKETPORT) + String("..."));
  webSocket.begin();
  USE_SERIAL.print(".");
  webSocket.onEvent(webSocketEvent);
  ledController.setFixedColor(successColor);
  delay(100);
  ledController.setFixedColor(loadingColor1);
  USE_SERIAL.println("OK!");

  ledController.setFixedColor(successColor);
  delay(500);
  ledController.setFixedColor(rgbColorWhite);
  delay(500);
  ledController.setFixedColor(successColor);
  delay(500);
  ledController.setFixedColor(rgbColorWhite);

  USE_SERIAL.print(String("Initializing UDP API on port ") + String(UDPAPIPORT) + String("..."));
  if(Udp.begin(UDPAPIPORT)) {
    USE_SERIAL.println("OK!");
  } else {
    USE_SERIAL.println("FAIL");
  }

  USE_SERIAL.println("\nALL SYSTEMS INITIALIZED\n");
}

void process_udp() {
  int udpPacketSize = Udp.parsePacket();
  if(udpPacketSize) {
    USE_SERIAL.printf("Received %d bytes from %s, port %d\n", udpPacketSize, Udp.remoteIP().toString().c_str(), Udp.remotePort());
    int len = Udp.read(udpPacketBuf, UDP_PACKET_BUFFER_SIZE);
    if(len > 0) {
      udpPacketBuf[len] = 0;
    }
    Serial.printf("UDP packet contents: %s\n", udpPacketBuf);
    char requestedCommand[255];
    int els = sscanf((const char*) udpPacketBuf, "%s", requestedCommand);
    if(els == 1) {
      // Got a command
    }
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    char replyPacket[] = "ACK";
    Udp.write(replyPacket);
    Udp.endPacket();
  }
}

void loop() {
  #ifdef USEOTA
    ArduinoOTA.handle();
    yield();
  #endif
  process_udp();
  ledController.run();
  yield();
  webSocket.loop();
  yield();
  server.handleClient();
  yield();
}
