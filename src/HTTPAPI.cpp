/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, May 2018
 **************************************************************************/
#ifndef _WIFIRGB__HTTPAPI_CPP
#define _WIFIRGB__HTTPAPI_CPP

#include "HTTPAPI.h"

void handleRoot() {
  char temp[400];
  int sec = millis() / 1000;
  int min = sec / 60;
  int hr = min / 60;

  snprintf(temp,400,
             "<html>\
  <head>\
    <meta http-equiv='refresh' content='5'/>\
    <title>ESP8266 Webserver</title>\
    <style>\
      body { background-color: #cccccc; font-family: Arial, Helvetica, Sans-Serif; Color: #000088; }\
    </style>\
  </head>\
  <body>\
    <h1>ESP8266 webserver up and running!</h1>\
    <p>Uptime: %02d:%02d:%02d</p>\
  </body>\
</html>", hr, min % 60, sec % 60);
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send ( 200, "text/html", temp );
}

void handleNotFound() {
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += ( server.method() == HTTP_GET ) ? "GET" : "POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";

  for ( uint8_t i = 0; i < server.args(); i++ ) {
    message += " " + server.argName ( i ) + ": " + server.arg ( i ) + "\n";
  }

  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send ( 404, "text/plain", message );
}

void handleGetState() {
  RGB currentRGB = ledController.getCurrentRGB();
  String res = "{\n";
  res += "\t\"code\": 200,";
  res += "\t\"channel\": {\n";
  res += "\t\t\"red\": " + String(currentRGB.red) + ",\n";
  res += "\t\t\"green\": " + String(currentRGB.green) + ",\n";
  res += "\t\t\"blue\": " + String(currentRGB.blue) + "\n";
  res += "\t}\n";
  res += "}\n";
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "application/json", res);
}

void handleApiSetColor() {
  float requestedRed = server.arg("channelR").toFloat();
  float requestedGreen = server.arg("channelG").toFloat();
  float requestedBlue= server.arg("channelB").toFloat();
  if(requestedRed < 0 || requestedRed > 1
      || requestedGreen < 0 || requestedGreen > 1
      || requestedBlue < 0 || requestedBlue > 1)
  {
    String message = "{\"message\": \"value(s) out of bounds\"}";
    server.sendHeader("Access-Control-Allow-Origin", "*");
    server.send(400, "application/json", message);
  }
  else
  {
    ledController.setFixedColor(requestedRed,
           requestedGreen,
           requestedBlue);
    String message = "{\"message\": \"ok\"}";
    server.sendHeader("Access-Control-Allow-Origin", "*");
    server.send(200, "application/json", message);
  }
}

void handleApiAnimationBreathe() {
  float color1R, color1G, color1B, color2R, color2G, color2B;
  int speed;
  color1R = server.arg("color1R").toFloat();
  color1G = server.arg("color1G").toFloat();
  color1B = server.arg("color1B").toFloat();
  color2R = server.arg("color2R").toFloat();
  color2G = server.arg("color2G").toFloat();
  color2B = server.arg("color2B").toFloat();
  speed = server.arg("speed").toInt();
  //USE_SERIAL.println(String("Got request to start breathing animation with speed ")+String(speed)+String(" between colors (")+String(color1R)+String(",")+String(color1G)+String(",")+String(color1B)+String("), (")+String(color2R)+String(",")+String(color2G)+String(",")+String(color2B)+String(")"));
  RGB color1 = RGB{color1R, color1G, color1B};
  RGB color2 = RGB{color2R, color2G, color2B};
  ledController.breathe(color1, color2, speed);
  String message = "{\"message\": \"ok\"}";
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "application/json", message);
}

void handleRSSI() {
  String response = "";
  response += String("{\"message\":") + String("\"RSSI:") + String(WiFi.RSSI()) + String("\"}");
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "application/json", response);
}

void handleApiSysGetLocalIp() {
  String response = String("{\"data\":{\"ip\":\"") + WiFi.localIP().toString()+ String("\"}}");
  server.sendHeader("Access-Control-Allow-Origin", "*");
  server.send(200, "application/json", response);
}

#endif
