/**************************************************************************
   Copyright (C) Axel Latvala - All Rights Reserved
   Unauthorized copying of this file, via any medium is strictly prohibited
   Proprietary and confidential
   Written by Axel Latvala <softwarelicense@alatvala.fi>, May 2018
 **************************************************************************/
#ifndef _WIFIRGB__WSAPI_CPP
#define _WIFIRGB__WSAPI_CPP

#include "WSAPI.h"

void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t length) {
  //USE_SERIAL.println("webSocketEvent");
  switch (type) {
    case WStype_DISCONNECTED:
      USE_SERIAL.printf("[%u] Disconnected!\n", num);
      break;
    case WStype_CONNECTED: {
        IPAddress ip = webSocket.remoteIP(num);
        USE_SERIAL.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num,
                          ip[0], ip[1], ip[2], ip[3], payload);

        // send message to client
        //webSocket.sendTXT(num, "Connected");
      }
      break;
    case WStype_TEXT: {
      //USE_SERIAL.printf("[%u] get Text: %s\n", num, payload);

      // JSON Parsing
      if(length > MAX_JSON_PAYLOAD_SIZE) {
        webSocket.sendTXT(num, "{\"code\":400,\"msg\":\"content too long\"}");
        break;
      }
      //USE_SERIAL.print("Parsing JSON...");

      StaticJsonBuffer<MAX_JSON_PAYLOAD_SIZE> jsonBuffer;
      JsonObject& requestJson = jsonBuffer.parseObject(payload);

      if(!requestJson.success()) {
        //USE_SERIAL.println("FAILED");
        webSocket.sendTXT(num, "{\"code\":400,\"msg\":\"invalid json\"}");
        break;
      }
      if(!requestJson.containsKey(WSAPI_CMD_KEY)) {
        //USE_SERIAL.println("OK!");
        webSocket.sendTXT(num, "{\"code\":400,\"msg\":\"illegal json\"}");
        break;
      }
      //USE_SERIAL.println("OK!");
      //USE_SERIAL.print("Got cmd ");
      //USE_SERIAL.println((const char*)requestJson["cmd"]);

      handleWSAPI(num, requestJson);

      // send message to client
      //webSocket.sendTXT(num, "Hello there ^_^");

      // send data to all connected clients
      //webSocket.broadcastTXT("You are all my buddies!");
      }
      break;
    case WStype_BIN:
      USE_SERIAL.printf("[%u] get binary length: %u\n", num, length);
      hexdump(payload, length);

      // send message to client
      // webSocket.sendBIN(num, payload, length);
    case WStype_ERROR:
      USE_SERIAL.printf("WebSocket ERROR: %s", payload);
      break;
    default:
      USE_SERIAL.printf("Unhandled websocket event: %x", type);
  }
}

void handleWSAPI(uint8_t clientNum, JsonObject& requestJson) {
  //USE_SERIAL.println("handleWSAPI");
  //yield();
  String cmd = requestJson["cmd"];
  if(cmd == "setColor") {
    float r = requestJson["channel"]["r"];
    float g = requestJson["channel"]["g"];
    float b = requestJson["channel"]["b"];
    if(r >= 0 && r <= 1
       && g >= 0 && g <= 1
       && b >= 0 && b <= 1) {
      ledController.setFixedColor(requestJson["channel"]["r"], requestJson["channel"]["g"], requestJson["channel"]["b"]);
      /*yield();
      webSocket.sendTXT(clientNum, "{\"code\":200,\"msg\":\"ok\"}");
      yield();*/
      // TODO: broadcast change to all clients (except this one?)
    }
    else {
      webSocket.sendTXT(clientNum, "{\"code\":405,\"msg\":\"bad request; keys r, g and b should exist in root/channel, and have values in range [0,1]\"}");
    }

  } else if(cmd == "getState") {
    RGB currentRGB = ledController.getCurrentRGB();
    String res = "{\n";
    res += "\t\"code\": 200,";
    res += "\t\"channel\": {\n";
    res += "\t\t\"red\": " + String(currentRGB.red) + ",\n";
    res += "\t\t\"green\": " + String(currentRGB.green) + ",\n";
    res += "\t\t\"blue\": " + String(currentRGB.blue) + "\n";
    res += "\t}\n";
    res += "}\n";
    webSocket.sendTXT(clientNum, res);
  } else {
    webSocket.sendTXT(clientNum, String("{\"code\":400,\"msg\":\"Unrecognized command '")+cmd+String("'\"}"));
  }
}

#endif
